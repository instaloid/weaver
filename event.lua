local Event = {}
Event.__index = Event

--[[ Keep a pool of listeners around to pull from instead of allocating new tables every time
     We COULD keep multiple tables on each event that would hold single values that we need and not whole tables
     but that's kinda ugly IMO. ]]--
local listenerPool = {}

local function grabListener()
	local listener
	-- no more listeners in the pool, make a new one
	if #listenerPool == 0 then
		listener = {}
	else
		-- there was a listener in the pool, grab it remove it from the list
		listener = listenerPool[#listenerPool]
		listenerPool[#listenerPool] = nil 
	end
	return listener
end

local function returnListener(listener)
	-- Make sure to nil any references this listener could still have
	listener.func = nil
	listener.tbl = nil
	listenerPool[#listenerPool+1] = listener
end

function Event:fire(...)
	local listeners = self.listeners
	local count = #self.listeners
	local i = 1
	while i <= count do
		local listener = listeners[i]
		local stop = false

		-- if the listener was added for an instance of a class, call it with that instance as the first argument
		if listener.tbl then
			stop = listener.func(listener.tbl, ...)
		else
			stop = listener.func(...)
		end

		-- remove and return the listener if it was a oneshot
		if listener.oneshot then
			count = count - 1
			table.remove(listeners, i)
			returnListener(listener)
		else
			-- otherwise just advance to the next listener
			i = i + 1
		end
		if stop then
			return
		end
	end
end

function Event:addListener(func, tbl, oneshot)
	assert(func, "No function given to listener")
	local listener = grabListener()
	listener.func = func
	listener.tbl = tbl or nil
	listener.oneshot = oneshot or false
	self.listeners[#self.listeners+1] = listener
end

function Event:removeListener(func, tbl)
	assert(func, "Trying to remove nil function from event")
	local listener = 0
	for i=1,#self.listeners do
		local l = self.listeners[i]
		if l.func == func and l.tbl == tbl then
			listener = i
			break
		end
	end
	assert(listener > 0, "No listener found with the given function and table")
	returnListener(table.remove(self.listeners, listener))
end

function Event:__call()
	return setmetatable({
		listeners = {}
	}, Event)
end

-- tbh this might not even work in any version of Lua < 5.2, and probably not even in LuaJIT until 3.0
-- but just in case this is running in one of those versions, make sure we return any listeners we were using
-- once this Event gets freed by the garbage collector
function Event:__gc()
	local listeners = #self.listeners
	for i=1,#listeners do
		returnListener(listeners[i])
	end
end

return setmetatable(Event, Event)