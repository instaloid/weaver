local Fabric = {}
Fabric.__index = Fabric

-- Find the Fiber module by using the path Fabric was required from
local path = (...):match("(.-)[^%.]+$")
local Fiber = require(path .. "fiber")

function Fabric:start(func, ...)
	local fiber = Fiber(func)

	-- resume the coroutine at least once so we can test if it even waits once or not
	-- if not we can just ignore it. also makes it work like Unity or Godot's coroutines
	fiber:resume(...)

	if fiber:status() ~= "dead" then
		self.fibers[#self.fibers+1] = fiber
		fiber.fabric = self
	end

	return fiber
end

function Fabric:update(dt)
	local fibers = self.fibers
	if #fibers == 0 then
		return
	end

	local i = 1
	local count = #fibers
	while i <= count do
		local fiber = fibers[i]
		if not fiber.paused then
			fiber:resume(dt or 0)
			if fiber:isDead() then
				-- It might be faster to just replace our current index with the fiber at the end of our
				-- fibers table, but this preserves the order fibers are resumed in and that's more important IMO.
				table.remove(fibers, i)
				count = count - 1
			else
				i = i + 1
			end
		else
			i = i + 1
		end
	end
end

function Fabric:pauseAll()
	-- now we COULD just have a single boolean, controlling whether or not this fabric will try and resume its fibers
	-- at all, but just manually pausing every fiber is more flexible. So we do that instead.
	for i=1, #self.fibers do
		self.fibers[i].paused = true
	end
end

function Fabric:unpauseAll()
	for i=1, #self.fibers do
		self.fibers[i].paused = false
	end
end

function Fabric:stopAll()
	for i=1, #self.fibers do
		-- Just forgetting about all fibers is the best we can do
		self.fibers[i] = nil
	end
end

function Fabric:__call()
	return setmetatable({
		fibers = {}
	}, Fabric)
end

-- Yeah I can be my own metatable. So what? You wanna fight about it?
return setmetatable(Fabric, Fabric)