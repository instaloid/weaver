# Weaver
A small library for events and easier cooperative multitasking for games made with [LÖVE.](https://love2d.org/) Can also be used with any Lua project, since it's just an abstraction of Lua's coroutines, but this whole readme assumes you're using LÖVE.

## Getting Started

```Lua
-- Both modules return the class with a __call metamethod to create a new instance
local Fabric = require "wvr.fabric"
local Fiber = require "wvr.fiber"
```
You can create Fabrics to manage groups of individual Fibers for you:
```Lua
-- Function our new fiber will run
local function fiberFunc()
	print("prints immediately")
	Fiber.wait()
	print("printed after resume")
end

local fabric = Fabric()
fabric:newFiber(fiberFunc)
```
And then in your tick/frame/step/update function:
```Lua
function love.update(dt)
	-- Prints 'prints immediately` the first update,
	-- and 'prints after resume' in the next one.
	fabric:update(dt)
end
```
To resume every Fiber until they either finish normally or call `wait()` again. Alternately, you can manually create and manage Fibers yourself:
```Lua
local fiber = Fiber(fiberFunc)
```
This creates a new Fiber in a suspended state. To actually start the Fiber, call `fiber:resume()` manually like so:
```Lua
fiber:resume()
-- Prints 'prints immediately' and waits
fiber:resume()
-- Prints 'printed after resume' and dies
```

Events and their listeners are provided as well, and can be required the same way as Fibers and Fabrics:
```Lua
local Event = require "wvr.event"
```

Events themselves need nothing special, only to be created and stored wherever you need one:
```Lua
local damaged = Event()
local healed = Event()
```

Other systems or classes can add their own listeners to these events to be notified when they're fired:
```Lua
local function damagePlayer(amount)
	player.health = player.health - amount
	damaged:fire(amount)
	print("remaining health: " .. tostring(player.health))
end

local function damageListener(amount)
	print("took " .. tostring(amount) .. " damage!")
end

damaged:addListener(damageListener)

damagePlayer(10)
-- Prints 'took 10 damage!' from the listener
-- and then prints the remaining health from the line after fire()
```

## Installation

Just download or clone this repository and put it in your project's directory. Alternately, add it as a submodule to your existing repository.

Your project's directory should look something like:
- src/
	- wvr/
		- fabric.lua
		- fiber.lua
		- event.lua
		- tests.lua, but you can safely get rid of it
	- main.lua

If you have your modules kept in a folder separate from your `main.lua`, you might want to do something like this at the start of your main file:
```Lua
-- Add module folder to package path
love.filesystem.setRequirePath(love.filesystem.getRequirePath() .. ";module/?.lua")
```
Where `";module/?.lua"` includes the path to your module folder. In this case, your project directory can look like:
- src/
	- module/
		- wvr/
			- fabric.lua
			- fiber.lua
			- event.lua
			- tests.lua
  - main.lua

## [Fiber](./fiber.lua)
A Fiber is just a thin wrapper/abstraction over Lua's coroutines, while Fabrics are what contain the *meat* of this library.

A Fiber is created and given a function, then `fiber:resume(...)` is called with any arguments you want to pass to its function, and you must then yield control back to the main thread with `wait(...)`. Any subsequent calls to `fiber:resume(...)` will cause the last `coroutine.yield(...)` call to return the arguments passed to it, which `wait(...)` uses internally. Because of all this abstraction, it's heavily recommended you use Fabrics to manage Fibers for you instead of creating them yourself.

- ### Functions
	- `boolean Fiber:isDead()`:  
		Returns whether or not the Fiber has run its function to completion.

	- `string Fiber:status()`:  
		Returns a string describing the status of the Fiber. Literally equivalent to `coroutine.status(thread)`.
		
		The possible values and what they mean are:
		- `'normal'`: The Fiber is alive, but has resumed another Fiber and is waiting for it to yield or finish.
		- `'running'`: The Fiber is alive and is currently running.
		- `'suspended'`: The Fiber is alive but has yielded and is waiting to be resumed.
		- `'dead'`: The Fiber has finished its function.

	- `... fiber:resume(...)`:  
		Resumes execution of the Fiber from the last `wait(...)`, or from the start of the Fiber's function if it's being called for the first time. If the fiber dies after this call, anything its function returned will be returned by this function as well.

	- `... Fiber.wait(...)`:  
		Pauses execution of the fiber to let other code run. All the possible arguments are:
		- `nil`: The Fiber will just wait until the next time its resumed, and will return anything passed to the next `resume`.
		- `number`: The Fiber will wait for this number of seconds before continuing. This is done by a Fabric passing the time passed since last `resume`.
		- `Fiber`: The Fiber will wait until the given fiber has finished execution completely, i.e, it's dead. Will also return anything the fiber's function returned as well.
		- `Event`: The Fiber will wait until the given Event has fired before continuing. If the event was fired with any arguments, those will be returned by `wait`.  

		Unlike the other functions, take care to only call this with the `.` syntax. This is like a static function and apart of the Fiber class itself, not instances of it. I won't pollute your global table for you, but honestly just take this and put it in there. It'll make your life so much easier.

	- `nil Fiber:pause()`:  
		Pauses the fiber, preventing it from resuming via a Fabric next update. Can still be resumed manually.

	- `nil Fiber:unpause()`:  
		Unpauses the fiber. Can't be called resume, as much as I wanted to, because that's already a function.
- ### Variables
	- `thread 'thread'`:  
		The underlying Lua thread owned by this fiber. You could grab this and use the `coroutine.*` functions with it yourself if you wanted to.

	- `boolean 'paused'`:  
		Whether or not this fiber will be skipped next time its fabric updates. Not very useful if you don't use fabrics though.
	
	- `table 'ret'`:  
		Contains the values returned from this fiber when its function ended, indexed as an array starting from 1. `nil` if the fiber's function didn't return anything.
	
	- `Fabric 'fabric'`:  
		The fabric that owns this fiber. Only exists if a fiber was started by a fabric, and it didn't immediately return.

## [Fabric](./fabric.lua)
A Fabric is a collection of Fibers that will be resumed with each call to `fabric:update(dt)`. Meant to be used in a game loop.

- ### Functions
	- `Fiber Fabric:start(func, ...)`:  
		Creates a new fiber, resumes it once, adds it to this fabric's internal list of fibers if it didn't finish on the first call, and returns it. Specifically in that order.

		But note, that because of that order, a fiber that creates a new fiber before waiting will be added to the list *after* the fiber it creates. Be careful!

	- `nil Fabric:update(dt)`:  
		Resumes every fiber this fabric contains in the order they were added. `dt` is passed to the fibers and returned from their last `coroutine.yield()` if they need them.

	- `nil Fabric:pauseAll()`:  
		Pauses every fiber this fabric has. A paused fiber will not be resumed on the next call to `Fabric:update(dt)`, but can still be resumed manually.

	- `nil Fabric:unpauseAll()`:  
		Opposite of `Fabric:pauseAll()`.

	- `nil Fabric:stopAll()`

- ### Variables
	- `table 'fibers'`:  
	The internal list of fibers this fabric contains, indexed as an array starting from 1. If for whatever reason you need to access this, it's here, but it's recommended to use `Fabric:update(dt)` if you need to resume fibers.

## [Event](./event.lua)
An Event is

Uh

Y'know, an Event. It can be created and fired off whenever needed, say the player clicks a button or gets hit by an enemy, and anything else that wants to know about that can listen and be notified when it happens.

- ### Functions
	- `nil Event:fire(...)`:  
		Fires the event, calling the function of every listener in the order they were added, passing the `...` arguments to them. If a listener's function returns `true` then the propagation stops and any listeners after it will not have their functions called and any oneshot listeners won't be removed.

	- `nil Event:addListener(function, table, oneshot)`:  
		Adds a new listener to this Event, using the given function and table. If `oneshot` is `true`, the listener is removed the next time this Event is fired.

		The function is called with the arguments from `Event:fire(...)`, but if `table` is not `nil` then the function is called with `table` as its first argument instead, letting you add listeners for instances of a class.

	- `nil Event:removeListener(function, table)`:  
		Removes the first listener that has the given function and table, if one is found.

- ### Variables
	- `table 'listeners'`:  
		The functions listening to this event, indexed as an array starting from 1. Each one is made up of the following keys:
		
		- `function 'func'`: The function to be called when the event fires.
		- `table 'tbl'`: The table to pass as the `self` argument of the function.
		- `boolean 'oneshot'`: Whether or not this listener is removed once the Event fires.

		Instead of just creating and destroying these tables whenever needed, the Event class itself keeps a table of listeners to be grabbed and released when an Event needs them, so you don't need to worry as much about garbage.

## Miscellaneous details
You can make new fibers from any table's function too, and even make it work with Lua's `:` syntax you're likely already using if you're doing something more object oriented.
```Lua
local Animal = {}
function Animal:speak()
	print(self.sound)
end
...

local cow = Animal:new()
cow.sound = "Moo"

fabric:newFiber(Animal.speak, cow)
-- Prints 'Moo'
```

## Testing

Use LuaUnit.