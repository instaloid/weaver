local Fiber = {}
Fiber.__index = Fiber

local path = (...):match("(.-)[^%.]+$")
local Event = require(path .. "event")

function Fiber:status()
	return coroutine.status(self.thread)
end

function Fiber:isDead()
	return self:status() == "dead"
end

local function checkStatus(fiber, ...)
	-- first argument from coroutine.resume should always be whether or not there was an error when resuming
	-- second argument should always be the error message if it errored
	-- I wonder what assert does with any extra arguments if there isn't an error though? 🤔
	assert(select(1, ...))

	-- because there wasn't an error, continue to check if the fiber died
	if fiber:isDead() then
		-- if it did, and its function returned something, store it in a table on the fiber
		if select("#", ...) > 1 then
			fiber.ret = {select(2, ...)}
		end
	end
end

function Fiber:resume(...)
	-- to avoid allocating a new table for EVERY single resuming fiber we can just abuse the multiple arguments/returns
	-- feature Lua has and keep it all on the stack. I hope. Or at least if not, it's invisible to us lol
	checkStatus(self, coroutine.resume(self.thread, ...))

	if self:isDead() then
		if self.ret then
			return unpack(self.ret)
		end
	end
end

function Fiber:pause()
	self.paused = true
end

function Fiber:unpause()
	self.paused = false
end

function Fiber.wait(value)
	-- no argument at all, just wait til next resume
	if not value then
		return coroutine.yield()
	end
	local tp = type(value)
	if tp == "number" then
		-- it was a number, which means that's the minimum number of seconds we have to wait for
		while value > 0 do
			value = value - coroutine.yield()
		end
	elseif tp == "table" then
		local mt = getmetatable(value)
		if mt == Fiber then
			-- it was a table, AND it happened to be a Fiber. Wait until it dies and return whatever it did
			while not value:isDead() do
				coroutine.yield()
			end
			if value.ret then
				return unpack(value.ret)
			end
		elseif mt == Event then
			-- it was still a table, but it was an Event this time. Add a oneshot listener and resume after it,
			-- returning whatever arguments were passed to the event
			local waiting = true
			local ret = nil
			value:addListener(function(...)
				waiting = false
				if select("#", ...) > 0 then
					ret = {...}
				end
			end, nil, true)
			while waiting do
				coroutine.yield()
			end
			if ret then
				return unpack(ret)
			end
		end
	end
end

function Fiber:__call(func)
	assert(func, "no function given to new fiber")
	return setmetatable({
		thread = coroutine.create(func),
		paused = false,
		ret = false
	}, Fiber)
end

-- Remember! You can't have metamethods like __call without a metatable, so set ourselves as our own metatable. :)
return setmetatable(Fiber, Fiber)