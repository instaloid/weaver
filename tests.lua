local lu = require 'luaunit'
local Fiber = require 'fiber'
local Event = require 'event'
local Fabric = require 'fabric'

local function resumeFiber(fiber, ...)
	fiber:resume(...)
end

function testFiberArguments()
	local ox, oy, oz, of 
	local fiberFunc = function(x, y, z, fiber)
		ox = x
		oy = y
		oz = z
		of = fiber
	end
	local fiber = Fiber(fiberFunc)
	resumeFiber(fiber, 3, "Moo", nil, fiber)
	lu.assertIs(ox, 3)
	lu.assertIs(oy, "Moo")
	lu.assertIs(oz, nil)
	lu.assertIs(of, fiber)
end

function testFiberWaitEvent()
	local event = Event()
	local ox, oy, oz, of 
	local fiberFunc = function(x, y, z, fiber)
		Fiber.wait(event)
		ox = x
		oy = y
		oz = z
		of = fiber
	end
	local fiber = Fiber(fiberFunc)
	resumeFiber(fiber, 3, "Moo", nil, fiber)
	lu.assertIs(#event.listeners, 1)
	lu.assertIs(ox, nil)
	lu.assertIs(oy, nil)
	lu.assertIs(oz, nil)
	lu.assertIs(of, nil)
	event:fire()
	lu.assertIs(#event.listeners, 0)
	resumeFiber(fiber)
	lu.assertIs(ox, 3)
	lu.assertIs(oy, "Moo")
	lu.assertIs(oz, nil)
	lu.assertIs(of, fiber)
end

function testFiberReturns()
	local ox, oy, oz, ow
	local fabric = Fabric()
	local fiberFunc = function()
		local coFiber = fabric:start(function()
			Fiber.wait()
			return 3, "Moo", nil, false
		end)
		ox, oy, oz, ow = Fiber.wait(coFiber)
	end
	fabric:start(fiberFunc)
	lu.assertIs(ox, nil)
	lu.assertIs(oy, nil)
	lu.assertIs(oz, nil)
	lu.assertIs(ow, nil)
	fabric:update()
	lu.assertIs(ox, 3)
	lu.assertIs(oy, "Moo")
	lu.assertIs(oz, nil)
	lu.assertIs(ow, false)
end

function testFiberEventReturns()
	local ox, oy, oz, ow
	local fabric = Fabric()
	local event = Event()
	local fiberFunc = function()
		ox, oy, oz, ow = Fiber.wait(event)
	end
	fabric:start(fiberFunc)
	lu.assertIs(ox, nil)
	lu.assertIs(oy, nil)
	lu.assertIs(oz, nil)
	lu.assertIs(ow, nil)
	event:fire(3, "Moo", nil, false)
	fabric:update()
	lu.assertIs(ox, 3)
	lu.assertIs(oy, "Moo")
	lu.assertIs(oz, nil)
	lu.assertIs(ow, false)
end

function testEventListener()
	local ox, oy, oz
	local func = function(y, z)
		print(y, z)
		ox = 3
		oy = y
		oz = z
	end
	local event = Event()
	event:addListener(func)
	event:fire("Caw", true)
	lu.assertIs(ox, 3)
	lu.assertIs(oy, "Caw")
	lu.assertIs(oz, true)
end

function testEventTableListener()
	local tbl = {
		ox, oy, oz,
		func = function(self, y, z)
			print(y, z)
			self.ox = 3
			self.oy = y
			self.oz = z
		end
	}
	local event = Event()
	event:addListener(tbl.func, tbl)
	event:fire("Caw", true)
	lu.assertIs(tbl.ox, 3)
	lu.assertIs(tbl.oy, "Caw")
	lu.assertIs(tbl.oz, true)
end

function testEventOneshotListener()
	local ox, oy, oz
	local func = function(y, z)
		print(y, z)
		ox = 3
		oy = y
		oz = z
	end
	local event = Event()
	lu.assertIs(#event.listeners, 0)
	event:addListener(func, nil, true)
	lu.assertIs(#event.listeners, 1)
	event:fire("Caw", true)
	lu.assertIs(ox, 3)
	lu.assertIs(oy, "Caw")
	lu.assertIs(oz, true)
	lu.assertIs(#event.listeners, 0)
end

function testEventStopper()
	local ox, oy, oz
	local func = function(y, z)
		print(y, z)
		ox = 3
		oy = y
		oz = z
		return true
	end
	local func2 = function(y, z)
		ox = y
		oz = z + 1
	end
	local event = Event()
	event:addListener(func)
	event:addListener(func2)
	event:fire("Caw", 20)
	lu.assertIs(ox, 3)
	lu.assertIs(oy, "Caw")
	lu.assertIs(oz, 20)
end

function testEventStopperOneshot()
	local ox, oy, oz
	local func = function(y, z)
		print(y, z)
		ox = 3
		oy = y
		oz = z
		return true
	end
	local func2 = function(y, z)
		ox = y
		oz = z + 1
	end
	local event = Event()
	event:addListener(func, nil, true)
	event:addListener(func2)
	event:fire("Caw", 20)
	lu.assertIs(ox, 3)
	lu.assertIs(oy, "Caw")
	lu.assertIs(oz, 20)
	event:fire("Woof", 30)
	lu.assertIs(ox, "Woof")
	lu.assertIs(oz, 31)
end

os.exit(lu.LuaUnit.run())